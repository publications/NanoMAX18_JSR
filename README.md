# NanoMAX18_JSR

interactive plots and data for MS (in preparation)

[![Binder](https://mybinder.org/badge.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fpublications%2FNanoMAX18_JSR.git/48cbcc2390cf0fb8e85e8c8890ce693c6681d9fe?filepath=fig2.ipynb)

<a href="https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Fpublications%2FNanoMAX18_JSR.git/48cbcc2390cf0fb8e85e8c8890ce693c6681d9fe?filepath=fig2.ipynb" target="_blank">mybinder</a>
