
# coding: utf-8

# In[1]:


import h5py
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

fname = 'fig2.h5'
dset0 = '/data/simulation/original'
dset1 = '/data/simulation/scaled'
dset2 = '/data/simulation/waveguide'
dset3 = '/data/experiment/original'
dset4 = '/data/experiment/scaled'

file     = h5py.File(fname, 'r')
data_org = file[dset0]
data_sim = file[dset1]
data_wg  = file[dset2]
data_raw = file[dset3]
data_exp = file[dset4]

#file.close()

def modelk(x, cc, k, phi, m, b):
    return np.power(10, (cc*np.sin(k*x+phi) + m*x+b) )
def model(x, cc, phi, m, b):
    k=0.09
    return np.power(10, (cc*np.sin(k*x+phi) + m*x+b) )


# In[2]:


# secondary source size in µm; range: 1 … 60; experimental values for 5:5:50
ssa = 5

idx = int(np.floor(ssa))
if (idx <  0): idx =  0
if (idx > 60): idx = 60

org = data_org[idx]
sim = data_sim[idx]
wg  = data_wg[idx]

idx = int(np.floor((ssa-2.5)/5))
if (idx < 0): idx = 0
if (idx > 9): idx = 9
exp = data_exp[idx]
raw = data_raw[idx]
raw[:,0] = (raw[:,0]+8.3034)*1e6
raw[:,1] = (raw[:,1])*0.8e-6

if (idx == 3): # remove bad points at ssa=20
    exp[71,1] = -1
    exp[72,1] = -1
    exp[73,1] = -1
    exp[76,1] = -1
    exp = np.delete(exp, np.where(exp[:,1] == -1), axis=0)


fit = curve_fit(model, exp[61:81,0], exp[61:81,1], p0=[0.15, -1.35, -0.01, 0.01])
fix = np.linspace(200, 600, 201)
fiy = model(fix, *fit[0])
con = np.fabs(fit[0][0]) #/0.325 # maximum contrast of simulated data


#fit = curve_fit(modelk, sim[2100:2500,0], sim[2100:2500,1], p0=[0.15, 0.09, -1.35, -0.01, 0.01])
#fix = np.linspace(200, 600, 201)
#fiy = modelk(fix, *fit[0])
#con = np.fabs(fit[0][0]) #/0.325 # maximum contrast of simulated data


lab1 = 'simulation @ '+str(ssa)+' µm'
lab2 = 'WG convolved'
lab3 = 'ssa = '+str((idx+1)*5)+' µm'
lab4 = 'fit, contrast=' + '{:5.3f}'.format(con)

fig = plt.figure(figsize=(20,6))

fig.add_subplot(1,2,1)
plt.plot(org[:,0], org[:,1],       label=lab1)
plt.plot(raw[:,0], raw[:,1], 'k+', label=lab3)
plt.xlabel('lateral position, nm')
plt.ylabel('intensity')
plt.legend(loc='upper right')

fig.add_subplot(1,2,2)
plt.plot(sim[1800:2600,0],  sim[1800:2600,1],           label=lab1)
plt.plot(wg [1730:2530,0],  wg [1730:2530,1],           label=lab2)
plt.plot(exp[  61:   81,0], exp[  61:   81,1]/10, 'k+', label=lab3)
plt.plot(fix,               fiy              /10, 'k-', label=lab4)
plt.xlabel('lateral position, nm')
plt.ylabel('intensity')
plt.legend(loc='upper right')
plt.yscale('log')

plt.show()


# In[3]:


consim = np.empty([0, 2])

for ssa in np.arange(1, 50):
    idx = ssa-1
    sim = data_sim[idx]

    fit = curve_fit(modelk, sim[2100:2500,0], sim[2100:2500,1], p0=[0.15, 0.09, -1.35, -0.01, 0.01])

    fix = np.linspace(200, 600, 201)
    fiy = modelk(fix, *fit[0])
    con = np.fabs(fit[0][0]) /0.325 # maximum contrast of simulated data

    consim = np.vstack((consim, [ssa, con]))


    
conwg  = np.empty([0, 2])

for ssa in np.arange(1, 50):
    idx = ssa-1
    wg  = data_wg[idx]

    fit = curve_fit(modelk, wg[2100:2500,0], wg[2100:2500,1], p0=[0.15, 0.09, -1.35, -0.01, 0.01])

    fix = np.linspace(200, 600, 201)
    fiy = modelk(fix, *fit[0])
    con = np.fabs(fit[0][0]) /0.325 # maximum contrast of simulated data

    conwg = np.vstack((conwg, [ssa, con]))


    
conexp = np.empty([0, 2])

for ssa in np.arange(5, 50, 5):
    idx = int(np.floor((ssa-2.5)/5))
    if (idx < 0): idx = 0
    if (idx > 9): idx = 9
    exp = data_exp[idx]
    if (idx == 3): # remove bad points at ssa=20
        exp[71,1] = -1
        exp[72,1] = -1
        exp[73,1] = -1
        exp[76,1] = -1
        exp = np.delete(exp, np.where(exp[:,1] == -1), axis=0)

    fit = curve_fit(model, exp[61:81,0], exp[61:81,1], p0=[0.15, -1, -0.001, 1])

    fix = np.linspace(200, 600, 201)
    fiy = model(fix, *fit[0])
    con = np.fabs(fit[0][0]) /0.325 # maximum contrast of simulated data

    conexp = np.vstack((conexp, [ssa, con]))

    
fig = plt.figure(figsize=(20,12))
    
plt.plot(consim[:,0], consim[:,1],       label='simulated contrast')
plt.plot(conwg [:,0], conwg [:,1],       label='WG convolved contrast')
plt.plot(conexp[:,0], conexp[:,1], 'k+', label='experimental contrast')

plt.xlabel('secondary source size, µm')
plt.ylabel('normlaised contrast')
plt.legend(loc='upper right')

plt.show()

